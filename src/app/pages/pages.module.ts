import { NgModule } from "@angular/core";
import { PAGES_ROUTES } from './pages.routes';

//Components
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Graficas1Component } from './graficas1/graficas1.component';
import { ProgressComponent } from './progress/progress.component';

//Modulos
import { sharedModule } from '../shared/shared.module';

@NgModule({
    declarations:[
        PagesComponent,
        DashboardComponent,
        Graficas1Component,
        ProgressComponent
    ],
    exports:[
        DashboardComponent,
        Graficas1Component,
        ProgressComponent
    ],
    imports:[
        sharedModule,
        PAGES_ROUTES
    ]
})
export class PagesModule{ }